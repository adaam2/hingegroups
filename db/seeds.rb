# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
require 'faker'


Group.destroy_all
User.destroy_all

Comment.destroy_all

for k in 0..3
    group = Group.create({ name: Faker::Team.name })

    for i in 0..2
    	user = User.create({ name: Faker::Name.name, imageUrl: Faker::Avatar.image, age: rand(i + 1), group_id: group.id
 	})
    	for j in 0..1
			comment = Comment.create({ content: Faker::Lorem.sentence, user_id: user.id, group_id: group.id})

			for c in 0..1
				child_comment = Comment.create({ content: Faker::Lorem.sentence, user_id: user.id, group_id: group.id, parent_comment_id: comment.id })
			end
    	end

	end
end




#   Mayor.create(name: 'Emanuel', city: cities.first)
