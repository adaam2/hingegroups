class AddCommentsToGroup < ActiveRecord::Migration
  def change
  	add_reference :groups, :comments, index: true, foreign_key: true
  end
end
