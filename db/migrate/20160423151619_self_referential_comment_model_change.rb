class SelfReferentialCommentModelChange < ActiveRecord::Migration
  def change
  	add_column :comments, :parent_comment_id, :integer
  	add_foreign_key :comments, :parent_comment_id
  end
end
