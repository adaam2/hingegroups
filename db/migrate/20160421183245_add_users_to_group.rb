class AddUsersToGroup < ActiveRecord::Migration
  def change
    add_reference :groups, :users, index: true, foreign_key: true
  end
end
