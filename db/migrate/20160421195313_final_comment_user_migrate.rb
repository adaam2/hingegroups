class FinalCommentUserMigrate < ActiveRecord::Migration
  def change
  	add_column :comments, :user_id, :integer
  	add_foreign_key :comments, :user_id
  end
end
