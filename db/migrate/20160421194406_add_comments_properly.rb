class AddCommentsProperly < ActiveRecord::Migration
  def change
  	add_column :comments, :group_id, :integer
  	add_foreign_key :comments, :group_id
  end
end
