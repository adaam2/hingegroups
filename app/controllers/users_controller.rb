class UsersController < ApplicationController
  def index
  	@users = if params[:id]
        User.where(id: params[:id])
    else
        User.all()
    end
  end

  def create
    pre = User.where(name: params[:name])
    if pre.count < 1
      adam = User.new
      adam.name = params[:name]
      adam.age = params[:age]
      adam.imageUrl = params[:imageUrl]

      offset = rand(Group.count)

      rand_record = Group.offset(offset).first

      adam.group_id = rand_record.id

      adam.save

      render :json => adam
    else
      render :json => pre.first
    end
  end
end
