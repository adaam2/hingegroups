class CommentsController < ApplicationController
  def index
  	@comments = if params[:name]
        Comment.where('content like ?',"%#{params[:name]}%")
    else
        Comment.all()
    end
  end
end
