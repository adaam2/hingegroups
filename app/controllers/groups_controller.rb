class GroupsController < ApplicationController
  def index
  	@groups = if params[:name]
        Group.where('name like ?',"%#{params[:name]}%")
    else
        Group.all()
    end
  end

  def comments
  	 @comments = Comment.joins(:user).where(group_id: params[:group_id], parent_comment_id: nil)


  end

  def add_comment
    new_comment = Comment.new
    new_comment.content = params[:content]
    new_comment.group_id = params[:group_id]
    new_comment.user_id = params[:user_id]
    new_comment.parent_comment_id = params[:parent_comment_id]
    new_comment.date = params[:date]
    new_comment.save
    render :json => new_comment, :include => :user
  end
end
