class Group < ActiveRecord::Base
	has_many :users, dependent: :destroy
	has_many :comments, dependent: :destroy
end
