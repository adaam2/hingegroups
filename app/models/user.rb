class User < ActiveRecord::Base
	has_many :comments, dependent: :destroy
	belongs_to :group
end
