class Comment < ActiveRecord::Base
	belongs_to :group
	belongs_to :user, :class_name => "User", :foreign_key => "user_id"
	belongs_to :parent, :class_name => "Comment", :foreign_key => "parent_comment_id"
	has_many :child_comments, :class_name => "Comment", :foreign_key => "parent_comment_id"
end
