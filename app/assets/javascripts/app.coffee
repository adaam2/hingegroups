app = angular.module('hingeGroups',[
  'templates',
  'ngRoute',
  'ngResource',
  'controllers',
  'directives'
])

app.config([ '$routeProvider',
  ($routeProvider)->
    $routeProvider
      .when('/',
        templateUrl: "group.html"
        controller: 'GroupsController'
      )
      .when('/groups',
        templateUrl: "group.html"
        controller: 'GroupsController'
      )
      .when('/group/:name',
        templateUrl: "group.html"
        controller: 'GroupsController'
      )
      .when('/users',
        templateUrl: "user.html",
        controller:"UserController"
      )
      .when('/user/:id',
        templateUrl: "user.html",
        controller:"UserController")
])

app.run(['$rootScope', '$resource', ($rootScope, $resource)->
  User = $resource('/users/create')
  newUser = {
    name: 'Adam Bull',
    age: 26,
    imageUrl: 'https://pbs.twimg.com/profile_images/718431559334944768/BSTH72yW_400x400.jpg'
  }
  savedUser = User.save(newUser)

  $rootScope.user = savedUser
])

app.config ["$httpProvider", ($httpProvider) ->
  $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content')
]

# let's set up the directives necessary for the comments box
directives = angular.module('directives', [])

directives.directive("collection", ['$compile', ($compile) ->
  {
    restrict: 'E'
    replace: true
    scope: collection: '='
    template: '<ul class="comments"><member ng-repeat=\'member in collection\' member=\'member\'></member></ul>'
  }
])

directives.directive("stopEvent", ()->
  {
    restrict: 'A'
    link: (scope, element, attr) ->
      element.bind 'click', (e) ->
        e.stopPropagation()
        return
      return

  }
)

directives.directive("commentbox", ['$compile', '$timeout', '$rootScope', '$resource', ($compile, $timeout, $rootScope, $resource)->
  restrict: 'E',
  replace: true,
  scope: parent: '='
  template: '<div class="comment-box"><textarea class="comment-box" ng-model="newComment" placeholder="Write a comment">Hello</textarea><div class="button-group stacked"><a class="button stacked" ng-click="addCommentDir(parent,newComment)">Submit</a><a class="button stacked" ng-click="newComment=\'\'">Reset</a></div></div>'
  link: (scope, element, attrs) ->
    scope.addCommentDir = (parent, newComment)->
      Comment = $resource('/groups/comments/add')
      payload = {
        parent_comment_id: parent.id,
        content: newComment,
        user_id: $rootScope.user.id,
        group_id: parent.group_id
      }
      payload = Comment.save(payload)
      if scope.parent.child_comments != undefined
        scope.parent.child_comments.push payload
      else

        scope.parent.child_comments = []
        scope.parent.child_comments.push payload

      scope.newComment = ''
])

directives.directive("avatar", ['$compile', '$filter', ($compile, $filter)->
  restrict: 'E',
  replace: true,
  scope: user: '='
  template: '<div data-username="{{user.name}}" class="user-avatar"><img src="{{user.imageUrl}}"/></div>'
])

directives.directive("member",['$compile', ($compile)->
  {
    restrict: 'E'
    replace: true
    scope: member: '='
    template: '<li class="comment" ng-click="comment.show = !comment.show" data-commentid="{{member.id}}"><blockquote><avatar class="user-avatar" user=\'member.user\'></avatar><p>{{member.content}}</p><p class="comment-details"><span ng-if="member.created_at !== undefined"><strong>Date:</strong> {{member.created_at | date: \'EEEE, MMMM d, y HH:mm\'}}</span></p></blockquote></li>'
    link: (scope, element, attrs) ->
      element.append '<commentbox stop-event ng-show="comment.show" parent=\'member\'></commentbox>'
      if angular.isArray(scope.member.child_comments)
        element.append '<collection class="replies" collection=\'member.child_comments\'></collection>'

        $compile(element.contents()) scope
      return

}])


controllers = angular.module('controllers',[])

controllers.controller("HomeController", [ '$scope',
  ($scope)->
])

controllers.controller("GroupsController", [ '$scope', '$rootScope', '$timeout', '$routeParams', '$resource',
  ($scope, $rootScope, $timeout, $routeParams, $resource)->

    # Declare resource objects
    Group = $resource('/groups/:name', { name: "@name", format: 'json' })
    Comment = $resource('/groups/comments/:group_id', { group_id: "@group_id", format: 'json' })

    if $routeParams.name
      Group.query(name: $routeParams.name, (results)->
        group = results[0]
        Comment.query(group_id: group.id, (childResults)->

          group.comments = childResults
          $scope.group = group
        )
      )
    else
      Group.query((results)-> $scope.groups = results)
      $scope.list = true
])
controllers.controller("UserController", [ '$scope', '$routeParams', '$resource',
  ($scope, $routeParams, $resource)->
    User = $resource('/users/:id', { id: "@id", format: 'json' })
    if $routeParams.id
      User.query(id: $routeParams.id, (results)-> $scope.user = results[0])

    else
      User.query((results)-> $scope.users = results)
      $scope.list = true
])

