Rails.application.routes.draw do
  get 'comments/index'

  get 'users/index'

  get 'home/index'
  root 'home#index'

  get '/groups/', to: 'groups#index'
  get '/groups/:name', to: 'groups#index'
  get '/users/', to: 'users#index'
  get '/users/:id', to: 'users#index'
  get '/comments/', to: 'comments#index'
  get '/comments/:name', to: 'comments#index'

  #get comments for specific group
  get '/groups/comments/:group_id', to: 'groups#comments'

  post '/groups/comments/add', to: 'groups#add_comment'

  post '/users/create', to: 'users#create'
end
